cats = {}

for i in range(1, 101):
    cats[i] = False
    
for i in range(1, 101):
    for cat_n, hat in cats.items():
        if cat_n % i == 0:
            if cats[cat_n]:
                cats[cat_n] = False
            else:
                cats[cat_n] = True

for cat_n in cats:
    if cats[cat_n]:
        print(f"Cat number {cat_n} has a hat.")
