class Animal:
    hunger = 0
    position = 0
    fatigue = 0
    def __init__(self, name, size):
        self.name = name
        self.size = size

    def move(self, distance):
        self.position += distance
        self.fatigue += distance 
        self.hunger += distance 
        return f"{self.name} is now at position {self.position}"
    
    def feed(self):
        self.hunger -= 10
        if self.hunger <= 0:
            return f"{self.name} is full"
        else:
            return f"{self.name} is still hungry"
        
    def rest(self):
        self.fatigue = 0
        self.hunger = 0
        self.position = 0
        return f"The {self.name} is going back home to rest"

    def needs(self):
        hungry = ""
        tired = ""
        if self.hunger > 10:
            hungry = "is hungry"
        else:
            hungry = "is full"
            
        if self.fatigue > 30:
            tired = "need rest"
        else:
            tired = "is not tired"
        return f"{self.name} {hungry}, and  {tired}"

    def talk(self, sound=None):
        if sound is None:
            return f"..."
        return f"*{self.name} says* {sound}"

class Pigs(Animal):
    def talk(self, sound="Oink Oink"):
        return super().talk(sound)

class Cows(Animal):
    def talk(self, sound="Moo"):
        return super().talk(sound)
    
class Cats(Animal):
    def talk(self, sound="Meow"):
        return super().talk(sound)
    def hunt(self):
        self.position += 5
        return f"{self.name} is looking for prey, now at position {self.position}"
    
cat = Cats("Paw", "small")
cow = Cows("Cow", "big")
pig = Pigs("Pig", "medium")

print(f"Hello, i want to show you my cat. He's name is '{cat.name}'")
print(f"And he is a {cat.size} one\n")

print(cat.talk())
print(cat.move(15))
print(cat.hunt())
print(f"{cat.name} found a mouse")
print(cat.needs())
print(f"{cat.name} ate his prey")
print(cat.feed())

print(f"\nGuess i need to feed him more")
print(cat.feed())
print(cat.move(25))
print(cat.needs())
print(cat.rest())
print(cat.needs())

print(f"\n\nOn the fields {cow.talk()}")
print(f"And on the pigsty {pig.talk()}")
