hangman = (
    """







""",
    """






//\\
""",
    """
 |
 |
 |
 |
 |
 |
//\\
""",
    """
  _______
 |      
 |      
 |     
 |     
 |
 |
//\\
""",
    """
  _______
 |      |
 |      
 |     
 |     
 |
 |
//\\
""",
    """
  _______
 |      |
 |      O
 |     
 |     
 |
 |
//\\
""",
    """
  _______
 |      |
 |      O
 |      |
 |     
 |
 |
//\\
""",
    """
  _______
 |      |
 |      O
 |     /|
 |     
 |
 |
//\\
""",
    """
  _______
 |      |
 |      O
 |     /|\\
 |     
 |
 |
//\\
""",
    """
  _______
 |      |
 |      O
 |     /|\\
 |     / 
 |
 |
//\\
""",
    """
  _______
 |      |
 |      O
 |     /|\\
 |     / \\
 |
 |
//\\
""",
    )
g_over = len(hangman) - 1
word = input("Enter the word to be guessed: ").upper()
progress = "-" * len(word)
wrong = 0
used = []
print("You can quit byt typing 'exit'")

while wrong < g_over and progress != word:
    guess = input(f"\n{"_____" * 8}\nGuess one letter: ").upper()
          
    while guess in used:
        print(f"{guess} has been used already")
        guess = input("Try again: ").upper()

    used.append(guess)

    if guess in word:
        print(f"\nCorrect, '{guess}' is in the word")

        correct = ""
        for i in range(len(word)):
            if guess == word[i]:
                correct = correct + guess
            else:
                correct = correct + progress[i]
        progress = correct
    elif guess == "EXIT":
        break
    else:
        print(f"\n'{guess}' is not in the word!")
        wrong = wrong + 1
    print(hangman[wrong])
    print(f"So far you guessed: {progress}")
    print(f"Letters tried so far: {used}")

if wrong == g_over:
    print(hangman[wrong])
    print("Game over")
elif progress == word:
    print(f"\n{"_____" * 8}\n\nThe answer is: '{word}', Congratulations\n{"_____" * 8}")

input("Press enter to exit")
