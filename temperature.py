def convert_cel_to_far(C):
    F = float(C) * 9/5 + 32
    return F

def convert_far_to_cel(F):
    C = (float(F) - 32) * 5/9
    return C

C = input("Enter a temperature in degrees C: ")
print(f"{C} degrees C = {convert_cel_to_far(C):.2f} degrees F")

F = input("Enter a temperature in degrees F: ")
print(f"{F} degrees F = {convert_far_to_cel(F):.2f} degrees C")
