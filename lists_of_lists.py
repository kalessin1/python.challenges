def enrolment_stats(universities_list):
    students = []
    tuition = []
    for university in universities_list:
        students.append(university[1])
        tuition.append(university[2])
    return students, tuition

def mean(values):
    return sum(values) / len(values)

def median(values):
    values.sort()
    if len(values) % 2 == 1:
        cent_index = int(len(values) / 2)
        return values[cent_index]
    else:
        L_cent_index = len(values) - 1 // 2
        R_cent_index = len(values) + 1 // 2
        return (L_cent_index + R_cent_index) / 2
        
universities = [
    ["California Institute of Technology", 2175, 37704],
    ["Harvard", 19627, 39849],
    ["Massachusetts Institute of Technology", 10566, 40732],
    ["Princeton", 7802, 37000],
    ["Rice", 5879, 35551],
    ["Stanford", 19535, 40569],
    ["Yale", 11701, 40500],
]

total = enrolment_stats(universities)

print(f"Total students:   {sum(total[0]):,}")
print(f"Total tuition:  $ {sum(total[1]):,}")
print(f"Student mean:     {mean(total[0]):,.2f}")
print(f"Student median:   {median(total[0]):,}")
print(f"tuition mean:   $ {mean(total[1]):,.2f}")
print(f"Tuition median: $ {median(total[1]):,}")
