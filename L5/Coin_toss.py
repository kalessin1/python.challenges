import random

def coin_flip():
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"

flips = 0
num_trials = 10_000

for trails in range(num_trials):
    if coin_flip() == "heads":
        flips = flips + 1   #first flip count
        while coin_flip() == "heads":
            flips = flips + 1 #flips cout if still "heads"
        flips = flips + 1 #flip count when tails
    else:
        flips = flips + 1 #first flip count
        while coin_flip() == "tails":
            flips = flips + 1 #flips count if still "tails"
        flips = flips + 1 #flip count when heads

avg = flips / num_trials
print(f"average flips is {avg}.")
