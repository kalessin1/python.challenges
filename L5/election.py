import random

def reg_election(win_prob):
    if random.random() < win_prob:
        return "A"
    else:
        return "B"


def election(regional_chances):
    reg_win_A = 0
    for win_prob in regional_chances:
        if reg_election(win_prob) == "A":
            reg_win_A = reg_win_A + 1
    reg_win_B = len(regional_chances) - reg_win_A 
    if reg_win_A > reg_win_B:
        return "A"
    else:
        return "B"

win_A_by_reg = [0.87, 0.65, 0.17]
trials_num = 10_000

wins_A = 0
for trial in range(trials_num):
    if election(win_A_by_reg) == "A":
        wins_A = wins_A + 1

print(f"Chance that A wins is: {wins_A / trials_num}")
